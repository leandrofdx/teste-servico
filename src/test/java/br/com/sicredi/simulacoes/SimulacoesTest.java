package br.com.sicredi.simulacoes;

import org.junit.BeforeClass;
import org.junit.Test;

import br.com.sicredi.base.BasePage;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;

public class SimulacoesTest {
	
	BasePage base = new BasePage(); 
	
	@BeforeClass
	public static void setup() {
		RestAssured.baseURI = "http://localhost:8080/api";
	}
	
	@Test
	public void deveAdicionarNovaSimulacao() {
		RestAssured.given()
			.body(base.getNovaSimulacao())
			.contentType(ContentType.JSON)
		.when()
			.post("/v1/simulacoes")
		.then()
			.log().all()
			.statusCode(201)
		;
	}
	
	@Test
	public void deveRetornarTodasSimulacoes() {
		RestAssured.given()
		.when()
			.get("/v1/simulacoes")
		.then()
			.statusCode(200)
		;
	}
	
	@Test
	public void deveRetornarSimulacaoPorCPF() {
		RestAssured.given()
		.when()
			.get("/v1/simulacoes/17822386034")
		.then()
			.log().all()
			.statusCode(200)
		;
	}
	
	@Test
	public void deveAtualizarSimulacaoPorCPF() {		
		RestAssured.given()
			.body(base.simulacao("Fulano de Tal Atualizado", "66414919004", "1000", "3", false))
			.contentType(ContentType.JSON)
		.when()
			.put("/v1/simulacoes/66414919004")
		.then()
			.log().all()
			.statusCode(200)
		;
	}
	
	@Test
	public void deveRemoverSimulacaoPorID() {
		RestAssured.given()
		.when()
			.delete("/v1/simulacoes/13")
		.then()
			.statusCode(200)
			.assertThat()
		;
	}
}
