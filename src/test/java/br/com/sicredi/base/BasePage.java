package br.com.sicredi.base;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

public class BasePage {
	
	String cpf = Long.toString(deveGerarCPF());
	Object novaSimulacao = deveGerarSimulacao();
	
	
	public Map<String, Object> deveGerarSimulacao() {
		return simulacao("Fulano de Tal", this.cpf, "1200", "3", true);
	}
	
	public  Map<String, Object> simulacao(String nome, String cpf, String valor, String parcelas, Boolean seguro){
		Map<String,Object> simulacao = new HashMap<String,Object>();
		simulacao.put("nome", nome);
		simulacao.put("cpf", cpf); 
		simulacao.put("email", cpf+"@email.com");
		simulacao.put("valor", valor);
		simulacao.put("parcelas", parcelas);
		simulacao.put("seguro", seguro);
		
		return simulacao;
	}
	
	public long deveGerarCPF() {
		  ThreadLocalRandom random = ThreadLocalRandom.current();
		  return random.nextLong(10000000000L, 100000000000L);
	}

	public String getCpf() {
		return cpf;
	}

	public Object getNovaSimulacao() {
		return novaSimulacao;
	}
}