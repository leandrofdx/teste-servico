package br.com.sicredi.restricoes;

import org.hamcrest.CoreMatchers;
import org.junit.BeforeClass;
import org.junit.Test;

import io.restassured.RestAssured;

public class RestricoesTest {
	
	@BeforeClass
	public static void setup() {
		RestAssured.baseURI = "http://localhost:8080/api";
	}
	
	@Test
	public void deveConsultarCPFComRestricao() {
		RestAssured.given()
		.when()
			.get("/v1/restricoes/97093236014")
		.then()
			.log().all()
			.statusCode(200)
			.body("mensagem", CoreMatchers.is("O CPF 97093236014 tem problema"));
		;
	}
	
	@Test
	public void deveConsultarCPFSemRestricao() {
		RestAssured.given()
		.when()
			.get("/v1/restricoes/97093236013")
		.then()
			.log().all()
			.statusCode(204)
		;
	}
	
	@Test
	public void deveConsultarCPFInvalido() {
		RestAssured.given()
		.when()
			.get("/v1/restricoes/$%invalido")
		.then()
			.log().all()
			.statusCode(204)
		;
	}
	
	@Test
	public void deveConsultarRestricaoSemInformarParametro() {
		RestAssured.given()
		.log().all()
		.when()
			.get("/v1/restricoes/")
		.then()
			.log().all()
			.statusCode(404)
			.body("message", CoreMatchers.is("No message available"));
		;
	}
}